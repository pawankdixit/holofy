//
//  ImageLoader.swift
//  Holofy
//
//  Created by Pawan Dixit on 1/14/21.
//

import Foundation
import SwiftUI
import Combine

public final class ImageLoader {
    public static let shared = ImageLoader()

    private let cache: ImageCaching
    private lazy var backgroundQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 5
        return queue
    }()

    public init(cache: ImageCaching = ImageCache()) {
        self.cache = cache
    }

    public func loadImage(from url: URL) -> AnyPublisher<UIImage?, Never> {
        if let image = cache[url] { return Just(image).eraseToAnyPublisher() }
        
        return URLSession.shared.dataTaskPublisher(for: url)
            .map { UIImage(data: $0.data) }
            .replaceError(with: nil)
            .handleEvents(receiveOutput: {[unowned self] image in
                guard let image = image else { return }
                self.cache[url] = image
            })
            .subscribe(on: backgroundQueue)
            .receive(on: OperationQueue.main)
            .eraseToAnyPublisher()
    }
    
    public func loadThumbnail(from url: URL, completion: @escaping (UIImage?)->Void) {
        if let image = cache[url] { DispatchQueue.main.async { completion(image) } }
        
        backgroundQueue.addOperation {
            if let image = getThumbnailImage(forUrl: url) {
                self.cache[url] = image
                DispatchQueue.main.async {
                    completion(image)
                }
            }
        }
    }
}
