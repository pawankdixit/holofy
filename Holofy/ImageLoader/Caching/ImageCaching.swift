//
//  ImageCaching.swift
//  Holofy
//
//  Created by Pawan Dixit on 1/14/21.
//

import Foundation
import UIKit

public protocol ImageCaching: class {
    func insertImage(_ image: UIImage?, for url: URL)
    func image(for url: URL) -> UIImage?
    func removeImage(for url: URL)
    subscript(_ url: URL) -> UIImage? { get set }
}
