//
//  ImageCache.swift
//  Holofy
//
//  Created by Pawan Dixit on 1/14/21.
//

import Foundation
import UIKit

public final class ImageCache: ImageCaching {
    
    static let shared = ImageCache()

    private lazy var imageCache: NSCache<AnyObject, AnyObject> = {
        let cache = NSCache<AnyObject, AnyObject>()
        cache.countLimit = 200
        return cache
    }()
    
    private let lock = NSLock()

    public init() {}

    public func image(for url: URL) -> UIImage? {
        lock.lock(); defer { lock.unlock() }
        
        return imageCache.object(forKey: url as AnyObject) as? UIImage
    }

    public func insertImage(_ image: UIImage?, for url: URL) {
        guard let image = image else { return removeImage(for: url) }
        let decompressedImage = image

        lock.lock(); defer { lock.unlock() }
        imageCache.setObject(decompressedImage, forKey: url as AnyObject, cost: 1)
    }

    public func removeImage(for url: URL) {
        lock.lock(); defer { lock.unlock() }
        imageCache.removeObject(forKey: url as AnyObject)
    }

    public subscript(_ key: URL) -> UIImage? {
        get { image(for: key) }
        set { insertImage(newValue, for: key) }
    }
}
