//
//  ImgurAPI.swift
//  Holofy
//
//  Created by Pawan Dixit on 1/13/21.
//

import Foundation
import Combine

class ImgurAPI {
    
    static let ClientId = "64307f1bff447bb"
    
    private struct ImgurAPIResult<T: Codable>: Codable {
        let data: [T]
    }
    
    static func loadViral(page: Int) -> AnyPublisher<[ImgurGalleryItem], Error> {
        let url = URL(string: "https://api.imgur.com/3/gallery/hot/viral/\(page).json")!
        var loginRequest = URLRequest(url: url)
        loginRequest.httpMethod = "GET"
        loginRequest.setValue("Client-ID \(ClientId)", forHTTPHeaderField: "Authorization")
        return URLSession.shared
            .dataTaskPublisher(for: loginRequest)
            .tryMap { try JSONDecoder().decode(ImgurAPIResult<ImgurGalleryItem>.self, from: $0.data).data }
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
    
    static func loadComments(id: String) -> AnyPublisher<[ImgurComment], Error> {
        let url = URL(string: "https://api.imgur.com/3/gallery/\(id)/comments/best")!
        var loginRequest = URLRequest(url: url)
        loginRequest.httpMethod = "GET"
        loginRequest.setValue("Client-ID \(ClientId)", forHTTPHeaderField: "Authorization")
        return URLSession.shared
            .dataTaskPublisher(for: loginRequest)
            .tryMap { try JSONDecoder().decode(ImgurAPIResult<ImgurComment>.self, from: $0.data).data }
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
}
