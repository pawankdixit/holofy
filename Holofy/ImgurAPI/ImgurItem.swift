//
//  ImgurImage.swift
//  Holofy
//
//  Created by Pawan Dixit on 1/13/21.
//

import Foundation

struct ImgurGalleryItem: Codable, Identifiable, Equatable {
    let id: String
    let title: String?
    let description: String?
    let datetime: Int
    let link: String
    let is_album: Bool?
    let cover: String?
    let cover_height: Int?
    let cover_width: Int?
    let height: Int?
    let width: Int?
    let has_sound: Bool?
    let animated: Bool?
    let views: Int
    let ups: Int?
    let downs: Int?
    let favorite_count: Int?
    let mp4: String?
    let images: [ImgurGalleryItem]?
    let points: Int?
    let tags: [ImgurTag]?
    let section: String?
    let account_url: String?
}

struct ImgurTag: Codable, Equatable {
    let display_name: String
}

struct ImgurComment: Codable, Identifiable, Equatable {
    let id: Int
    let comment: String
    let author: String
    let ups: Int
    let downs: Int
}
