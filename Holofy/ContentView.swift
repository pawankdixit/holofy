//
//  ContentView.swift
//  Holofy
//
//  Created by Pawan Dixit on 1/13/21.
//

import SwiftUI

// Using the public apis only
//    let clientId = "64307f1bff447bb"
//    let clientSecret = "86ca7d37047e48ec0e2582ff5e00dd3c48b1e3fa"
//    let authHeader = "Authorization: Client-ID 64307f1bff447bb"

struct ContentView: View {
    var userSettings = UserSettings()
    
    var body: some View {
        HomeView(gallery: ViralGalleryViewModel.shared).environmentObject(userSettings)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
