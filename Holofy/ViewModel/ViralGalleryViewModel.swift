//
//  ViewModel.swift
//  Holofy
//
//  Created by Pawan Dixit on 1/13/21.
//

import Foundation
import Combine

class ViralGalleryViewModel: ObservableObject {
    
    static var shared = ViralGalleryViewModel()
    
    struct Model {
        var images: [ImgurGalleryItem] = []
        var bestComments = [String: [ImgurComment]]()
        var page: Int = 0
        var canLoadNextPage = true
        var lastLoaded = 0
    }
    
    private var tasks = Set<AnyCancellable>()
    @Published private(set) var model = Model()
    
    func fetchNextPageIfPossible() {
        guard model.canLoadNextPage else { return }
        guard model.images.count > model.lastLoaded || model.images.count == 0  else { return }
        
        model.lastLoaded = model.images.count
        
        ImgurAPI.loadViral(page: model.page)
            .sink(receiveCompletion: onReceive, receiveValue: onReceive)
            .store(in: &tasks)
    }
    
    // MARK: Private
    private func onReceive(_ completion: Subscribers.Completion<Error>) {
        switch completion {
        case .finished:
            break
        case .failure:
            model.canLoadNextPage = false
        }
    }

    private func onReceive(_ images: [ImgurGalleryItem]) {
        model.images += images
        model.page += 1
    }
    
    
    func fetchBestComments(id: String) {
        guard model.canLoadNextPage else { return }
        guard model.bestComments[id] == nil || model.bestComments[id]?.count == 0 else { return }
        
        ImgurAPI.loadComments(id: id)
            .sink(receiveCompletion: onReceive, receiveValue: { comments in
                self.model.bestComments[id] = comments
            })
            .store(in: &tasks)
    }
}
