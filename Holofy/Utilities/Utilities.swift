//
//  Extensions.swift
//  Holofy
//
//  Created by Pawan Dixit on 1/14/21.
//

import SwiftUI

struct GeometryGetter: ViewModifier {
    
    var completion: (GeometryProxy) -> Void
    
    func body(content: Content) -> some View {
        return GeometryReader { (g) -> Color in
            DispatchQueue.main.async {
                self.completion(g)
            }
            return Color.clear
        }
    }
}

extension View {
    func geometryReader(_ binding: Binding<CGRect>, in space: CoordinateSpace) -> some View {
        self.background(GeometryReader { (geometry) -> AnyView in
            let frame = geometry.frame(in: space)
            DispatchQueue.main.async {
                binding.wrappedValue = frame
            }
            return AnyView(Rectangle().fill(Color.clear))
        })
    }
}

extension UIDevice {
    var hasNotch: Bool {
        let bottom = UIApplication.shared.windows.first?.safeAreaInsets.bottom ?? 0
        return bottom > 0
    }
}

// Swipe to go back
extension UINavigationController: UIGestureRecognizerDelegate {
    override open func viewDidLoad() {
        super.viewDidLoad()
        interactivePopGestureRecognizer?.delegate = self
    }

    public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return viewControllers.count > 1
    }
}
