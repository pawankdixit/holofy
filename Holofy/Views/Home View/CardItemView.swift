//
//  StaggeredItemView.swift
//  Holofy
//
//  Created by Pawan Dixit on 1/15/21.
//

import SwiftUI

struct CardItemView: View {
    
    let imgurImage: ImgurGalleryItem
    var visibleRect: CGRect
    @State private var image: UIImage? = nil
    
    var body: some View {
        
        let firstImage = imgurImage.cover != nil ? imgurImage.images!.first! : imgurImage
        
        VStack(spacing: 0) {
            if image != nil {
                VStack(alignment: .leading, spacing: 10) {
                    HStack {
                        Text((imgurImage.tags?.first?.display_name ?? imgurImage.title) ?? "")
                            .bold()
                            .lineLimit(2)
                            .fixedSize(horizontal: false, vertical: true)
                        Spacer()
                        Image(systemName: "ellipsis")
                    }
                    .font(.subheadline)
                    
                    HStack {
                        Text(imgurImage.account_url ?? "").bold()
                            .foregroundColor(.gray)
                        Image(systemName: "circlebadge.fill").font(.system(size: 5))
                            .foregroundColor(.gray)
                        Text("follow").bold()
                            .foregroundColor(.white)
                        Spacer()
                        Text("\(Double(imgurImage.views)/1000.0, specifier: "%.1f")k")
                        Image(systemName: "eye.fill")
                    }
                    
                    .font(.headline)
                    
                }
                .padding()
                .foregroundColor(.white)
                .background(Color("PointsBar"))
            }
            
            SpringLoadedImageView(imgurImage: firstImage, visibleRect: visibleRect, image: $image)
        }
    }
}
