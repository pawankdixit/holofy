//
//  ItemView.swift
//  Holofy
//
//  Created by Pawan Dixit on 1/14/21.
//

import Foundation
import SwiftUI
import Combine

struct GalleryNavigationView: View {
    @EnvironmentObject var userSettings: UserSettings
    
    let imgurImage: ImgurGalleryItem
    var visibleRect: CGRect
    
    @State private var cancellable: AnyCancellable? = nil
    @State private var size = CGSize.zero
    @State private var image: UIImage? = nil
    
    var body: some View {
        NavigationLink(destination: MemeDetailView(imgurImage: imgurImage)) {
            Group {
                if userSettings.homeLayout == .waterfall {
                    StaggeredItemView(imgurImage: imgurImage, visibleRect: visibleRect)
                } else {
                    CardItemView(imgurImage: imgurImage, visibleRect: visibleRect)
                }
            }
            .cornerRadius(5)
        }
    }
}
