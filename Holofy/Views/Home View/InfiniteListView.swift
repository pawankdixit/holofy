//
//  List.swift
//  Holofy
//
//  Created by Pawan Dixit on 1/14/21.
//

import Foundation
import SwiftUI

struct InfiniteListView: View {
    @EnvironmentObject var userSettings: UserSettings
    
    let images: [ImgurGalleryItem]
    let isListLoading: Bool
    let onReachingEndOfFeed: () -> Void
    
    @State private var visibleRect: CGRect = .zero
    
    @State var currentIndex = 0
    let maxItemsOnScreen = 100
    
    var body: some View {
        ScrollView(.vertical) {
            Group {
                if userSettings.homeLayout == .waterfall {
                    // Staggered List
                    HStack(alignment: .top) {
                        VStack {
                            ForEach(images.indices.compactMap{$0 % 2 == 0 ? $0 : nil }, id:\.self) { index in
                                if index > max(currentIndex - maxItemsOnScreen, 0) && index < min(currentIndex + maxItemsOnScreen, images.count) {
                                    itemView(from: index)
                                }
                            }
                        }
                        VStack {
                            ForEach(images.indices.compactMap{$0 % 2 != 0 ? $0 : nil }, id:\.self) { index in
                                if index > max(currentIndex - maxItemsOnScreen, 0) && index < min(currentIndex + maxItemsOnScreen, images.count) {
                                    itemView(from: index)
                                }
                            }
                        }
                    }
                } else {
                    // Card View
                    ForEach(images.indices, id:\.self) { index in
                        if index > max(currentIndex - maxItemsOnScreen, 0) && index < min(currentIndex + maxItemsOnScreen, images.count) {
                            itemView(from: index)
                        }
                    }
                }
            }
            .padding(.top, 10)
            if isListLoading {
                loadingIndicator
            }
        }
        .coordinateSpace(name: "holofy")
        .geometryReader(self.$visibleRect, in: .named("holofy"))
    }
    
    private func itemView(from index: Int) -> some View {
        let image = images[index]
        
        return GalleryNavigationView(imgurImage: image, visibleRect: visibleRect)
            .environmentObject(userSettings)
            .overlay(Color.clear.modifier(GeometryGetter(){ proxy in
                guard proxy.size.height > 50 && proxy.size.width > 50 else { return }
                let rect = proxy.frame(in: .named("holofy"))
                if (rect.minY > self.visibleRect.minY && rect.minY < self.visibleRect.maxY) || (rect.maxY < self.visibleRect.maxY && rect.maxY > self.visibleRect.minY) {
                    // Item is visible on screen
                    currentIndex = index
                    if self.images.last == image {
                        self.onReachingEndOfFeed()
                    }
                }
            }))
    }
    
    private var loadingIndicator: some View {
        Spinner(style: .white)
            .frame(idealWidth: .infinity, maxWidth: .infinity, alignment: .center)
    }
}
