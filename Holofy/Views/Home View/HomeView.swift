//
//  View.swift
//  Holofy
//
//  Created by Pawan Dixit on 1/13/21.
//

import Foundation
import SwiftUI
import Combine

struct HomeView: View {
    @EnvironmentObject var userSettings: UserSettings
    
    @ObservedObject var gallery: ViralGalleryViewModel
    @State var isLoaded = false
    
    var body: some View {
        NavigationView {
            VStack(spacing: 0) {
                MainTopBar().environmentObject(userSettings)
                
                Group {
                    if userSettings.homeContent == .viral {
                        InfiniteListView(images: gallery.model.images,
                                        isListLoading: gallery.model.canLoadNextPage,
                                        onReachingEndOfFeed: gallery.fetchNextPageIfPossible)
                            .environmentObject(userSettings)
                    } else {
                        VStack {
                            Spacer()
                            Text("Not implemented").foregroundColor(.white)
                            Spacer()
                        }
                    }
                }
                .padding([.horizontal], 5)
                
                MainBottomBar()
            }
            .onAppear(perform: load)
            .navigationBarHidden(true)
            .background(Color.black.opacity(0.9))
            .edgesIgnoringSafeArea(.all)
        }
    }
    
    func load() {
        if !isLoaded {
            gallery.fetchNextPageIfPossible()
            isLoaded = true
        }
    }
}
