//
//  StaggeredItemView.swift
//  Holofy
//
//  Created by Pawan Dixit on 1/15/21.
//

import SwiftUI

struct StaggeredItemView: View {
    
    let imgurImage: ImgurGalleryItem
    var visibleRect: CGRect
    @State private var image: UIImage? = nil
    
    var body: some View {
        
        let firstImage = imgurImage.cover != nil ? imgurImage.images!.first! : imgurImage
        
        VStack(spacing: 0) {
            SpringLoadedImageView(imgurImage: firstImage, visibleRect: visibleRect, image: $image)
            
            if image != nil {
                VStack(alignment: .leading, spacing: 10) {
                    HStack {
                        Text((imgurImage.tags?.first?.display_name ?? imgurImage.title) ?? "")
                            .bold()
                            .lineLimit(2)
                            .fixedSize(horizontal: false, vertical: true)
                        Spacer()
                    }
                    .foregroundColor(.white)
                    .font(.subheadline)
                    
                    HStack {
                        Image(systemName: "arrow.up")
                            .font(.subheadline)
                        Text("\(imgurImage.points ?? 0)")
                            .bold()
                            .font(.caption)
                        Spacer()
                    }
                    .foregroundColor(.gray)
                }
                .padding()
                .background(Color("PointsBar"))
            }
        }
    }
}
