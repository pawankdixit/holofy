//
//  BottomBar.swift
//  Holofy
//
//  Created by Pawan Dixit on 1/14/21.
//

import SwiftUI

struct MainBottomBar: View {
    var body: some View {
        HStack {
            Image(systemName: "house.fill")
            Spacer()
            Image(systemName: "magnifyingglass")
            Spacer()
            Image(systemName: "tv")
            Spacer()
            Image(systemName: "bell")
            Spacer()
            Image(systemName: "person")
        }
        .padding()
        .padding(.bottom, UIDevice.current.hasNotch ? 10 : 0)
        .font(.system(.title))
        .foregroundColor(.white)
        .background(LinearGradient(gradient: Gradient(colors: [Color.gray.opacity(0.3), Color.gray.opacity(0.4)]), startPoint: .leading, endPoint: .trailing).shadow(color: .black, radius: 10, x: 0, y: -10))
    }
}
