//
//  BottomBar.swift
//  Holofy
//
//  Created by Pawan Dixit on 1/14/21.
//

import SwiftUI

struct DetailTopBar: View {
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    
    let imgurImage: ImgurGalleryItem

    var body: some View {
        HStack {
            Image(systemName: "arrow.backward")
                .onTapGesture {
                    self.mode.wrappedValue.dismiss()
                }
            
            VStack(alignment: .leading, spacing: 5) {
                HStack {
                    Text((imgurImage.tags?.first?.display_name ?? imgurImage.title) ?? "")
                        .bold()
                        .font(.headline)
                        .lineLimit(2)
                        .fixedSize(horizontal: false, vertical: true)
                    Spacer()
                    Image(systemName: "ellipsis")
                        .foregroundColor(.gray)
                }
                .font(.subheadline)
                
                HStack {
                    Text(imgurImage.account_url ?? "").bold()
                        .foregroundColor(.gray)
                    Image(systemName: "circlebadge.fill").font(.system(size: 5))
                        .foregroundColor(.gray)
                    Text("follow").bold()
                        .foregroundColor(.white)
                        .font(.subheadline)
                    Spacer()
                    Text("4h")
                        .foregroundColor(.gray)
                }
                .font(.caption)
                
            }
            .foregroundColor(.white)
        }
        .padding([.horizontal])
        .padding(.bottom, 10)
        .padding(.top, UIDevice.current.hasNotch ? 55 : 30)
        .foregroundColor(.white)
        .background(Color("Detail"))
    }
}
