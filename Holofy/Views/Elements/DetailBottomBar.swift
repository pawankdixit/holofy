//
//  BottomBar.swift
//  Holofy
//
//  Created by Pawan Dixit on 1/14/21.
//

import SwiftUI

struct DetailBottomBar: View {
    
    let imgurImage: ImgurGalleryItem
    
    var body: some View {
        HStack {
            Image(systemName: "arrow.up")
            Text("\(imgurImage.ups!)")
            Spacer()
            Image(systemName: "arrow.down")
            Text("\(imgurImage.downs!)")
            Spacer()
            Image(systemName: "heart")
            Text("\(imgurImage.favorite_count!)")
            Spacer()
            Image(systemName: "arrowshape.turn.up.forward")
        }
        .padding()
        .padding(.bottom, UIDevice.current.hasNotch ? 10 : 0)
        .font(.system(.headline))
        .foregroundColor(.white)
        .background(Color("DetailBottomBar").shadow(color: Color.black.opacity(0.2), radius: 5, x: 0, y: -5))
        
    }
}
