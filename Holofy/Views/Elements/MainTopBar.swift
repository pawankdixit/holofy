//
//  BottomBar.swift
//  Holofy
//
//  Created by Pawan Dixit on 1/14/21.
//

import SwiftUI

struct MainTopBar: View {
    @EnvironmentObject var userSettings: UserSettings
    
    @State var open = false
    @State var popoverSize = CGSize(width: 300, height: 300)
    
    var body: some View {
        HStack(spacing: 20) {
            WithPopover(showPopover: $open, popoverSize: popoverSize, content: {
                Button {
                    self.open.toggle()
                } label: {
                    Image(systemName: "arrow.up.arrow.down").font(.headline)
                }
            },
            popoverContent: {
                popover
            })
            Spacer()
            Text("Most Viral").bold()
                .onTapGesture {
                    userSettings.homeContent = .viral
                }
            Text("Feed").bold()
                .onTapGesture {
                    userSettings.homeContent = .feed
                }
            Spacer()
            Image(systemName: "plus.rectangle.fill").font(.headline)
                .shadow(color: Color.black.opacity(0.4), radius: 10, x: 0, y: 15)
        }
        .padding([.horizontal])
        .padding(.bottom, 10)
        .padding(.top, UIDevice.current.hasNotch ? 55 : 30)
        .foregroundColor(.white)
        .background(LinearGradient(gradient: Gradient(colors: [Color("MainTopBarLeft"), Color("MainTopBarRight")]), startPoint: .leading, endPoint: .trailing))
    }
    
    private var popover: some View {
        VStack(spacing: 20) {
            Spacer()
            HStack {
                VStack(alignment: .leading, spacing: 15) {
                    Text("MOST VIRAL").font(.subheadline).opacity(0.5)
                    Text("Popular")
                    Text("Random")
                    Text("Newest")
                }
                
                Spacer()
                
                VStack(alignment: .leading, spacing: 15) {
                    Text("USER SUBMITTED").font(.subheadline).opacity(0.5)
                    Text("Popular")
                    Text("Rising")
                    Text("Newest")
                }
            }
            .padding(.horizontal, 30)
            
            Divider().background(Color.black)
            
            HStack {
                Text("Layout").font(.subheadline).opacity(0.5)
                Spacer()
            }.padding(.horizontal, 30)
            
            HStack {
                VStack {
                    HStack {
                        Image(systemName: "rectangle.3.offgrid.fill")
                        Text("Waterfall")
                    }
                    .onTapGesture {
                        userSettings.homeLayout = .waterfall
                        self.open.toggle()
                    }
                }
                Spacer()
                VStack {
                    HStack {
                        Image(systemName: "photo.fill")
                        Text("Card")
                    }
                    .onTapGesture {
                        userSettings.homeLayout = .card
                        self.open.toggle()
                    }
                }
            }
            
            .padding(.horizontal, 30)
            Spacer()
        }
        .padding(.vertical, 5)
        .foregroundColor(.white)
        .font(.headline)
        .edgesIgnoringSafeArea(.all)
        .background(Color("Popover").edgesIgnoringSafeArea(.all))
    }
}
