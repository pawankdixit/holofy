//
//  ItemView.swift
//  Holofy
//
//  Created by Pawan Dixit on 1/14/21.
//

import Foundation
import SwiftUI
import Combine
import AVFoundation

struct PlayerView: UIViewRepresentable {
    let urlString: String
    func updateUIView(_ uiView: UIView, context: UIViewRepresentableContext<PlayerView>) {
    }
    
    func makeUIView(context: Context) -> UIView {
        return PlayerUIView(urlString: urlString, frame: .zero)
    }
}

class PlayerUIView: UIView {
    private let playerLayer = AVPlayerLayer()
    var loopPlayer: AVPlayerLooper!
    var queuePlayer: AVQueuePlayer!
    
    init(urlString:String, frame: CGRect) {
        super.init(frame: frame)
        
        let url = URL(string: urlString)!
        let asset = AVAsset(url: url)
        let item = AVPlayerItem(asset: asset)
        queuePlayer = AVQueuePlayer(playerItem: item)
        loopPlayer = AVPlayerLooper(player: queuePlayer, templateItem: item)
        queuePlayer.play()
        
        playerLayer.player = queuePlayer
        layer.addSublayer(playerLayer)
        
        NotificationCenter.default.addObserver(forName: Notification.Name(rawValue: "pause"), object: nil, queue: .main) { _ in
            self.queuePlayer?.pause()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        playerLayer.frame = bounds
    }
}
