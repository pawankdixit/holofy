//
//  ItemView.swift
//  Holofy
//
//  Created by Pawan Dixit on 1/14/21.
//

import Foundation
import SwiftUI
import Combine
import AVFoundation

// Image is loaded only when it's visible on screen
struct SpringLoadedImageView: View {
    
    let imgurImage: ImgurGalleryItem
    var visibleRect: CGRect = .zero
    var isVideoEnabled = false
    
    @State private var cancellable: AnyCancellable? = nil
    @State private var size = CGSize.zero
    @Binding var image: UIImage?
    @State var player = AVPlayer()
    
    var body: some View {
        let ratio = CGFloat(imgurImage.cover_width ?? imgurImage.width!) / size.width
        let width = CGFloat(imgurImage.cover_width ?? imgurImage.width!) / ratio
        let height = CGFloat(imgurImage.cover_height ?? imgurImage.height!) / ratio
        
        Group {
            // visibleRect is zero means we want to load the media immediately (used for detail view)
            // using the mp4 as it seems to best and can serve sound as well
            if isVideoEnabled && imgurImage.mp4 != nil {
                PlayerView(urlString: imgurImage.mp4!)
                    .frame(width: width, height: height)
                    .onDisappear() {
                        NotificationCenter.default.post(.init(name: Notification.Name(rawValue: "pause")))
                    }
            }
            else {
                ZStack {
                    Image(uiImage: image != nil ? image! : UIImage())
                        .resizable()
                    if image == nil && cancellable != nil {
                        Spinner(style: .white)
                    }
                }
                .frame(width: width, height: height)
                .overlay(Color.clear.modifier(GeometryGetter(){ proxy in
                    if isVideoEnabled {
                        startLoadingImage()
                    } else {
                        guard proxy.size.height > 50 && proxy.size.width > 50 else { return }
                        let rect = proxy.frame(in: .named("holofy"))
                        if image == nil {
                            if (rect.minY > self.visibleRect.minY && rect.minY < self.visibleRect.maxY) || (rect.maxY < self.visibleRect.maxY && rect.maxY > self.visibleRect.minY) {
                                // Image is visible on screen
                                startLoadingImage()
                            } else if cancellable != nil && abs(rect.minY) > 1.2 * UIScreen.main.bounds.height {
                                cancellable?.cancel()
                                image = nil
                                cancellable = nil
                            }
                        } else {
                            if abs(rect.minY) > 1.2 * UIScreen.main.bounds.height  {
                                image = nil
                                cancellable?.cancel()
                                cancellable = nil
                            }
                        }
                    }
                }))
            }
        }
        .frame(idealWidth: .infinity, maxWidth: .infinity, alignment: .center)
        .overlay(Color.clear.modifier(GeometryGetter(){ g in
            self.size = g.size
        }))
    }
    
    private func startLoadingImage() {
        DispatchQueue.global(qos: .background).async {
            if cancellable == nil {
                let url = !(imgurImage.is_album ?? false) ? URL(string: imgurImage.link)! : URL(string: imgurImage.images!.first!.link)!
                if url.absoluteString.contains("mp4") {
                    ImageLoader.shared.loadThumbnail(from: url) { image = $0 }
                    cancellable = .init({})
                } else {
                    cancellable = ImageLoader.shared.loadImage(from: url)
                        .sink { image = $0 }
                }
            }
        }
    }
}

func getThumbnailImage(forUrl url: URL) -> UIImage? {
    let asset: AVAsset = AVAsset(url: url)
    let imageGenerator = AVAssetImageGenerator(asset: asset)
    
    do {
        let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60) , actualTime: nil)
        return UIImage(cgImage: thumbnailImage)
    } catch let error {
        print(error)
    }
    
    return nil
}
