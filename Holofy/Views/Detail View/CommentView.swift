//
//  CommentView.swift
//  Holofy
//
//  Created by Pawan Dixit on 1/14/21.
//

import SwiftUI
import Combine

struct CommentView: View {
    
    let imgurComment: ImgurComment
    
    @State private var cancellable: AnyCancellable? = nil
    @State private var image: UIImage? = nil
    
    var body: some View {
        HStack(alignment: .top) {
            VStack {
                Image(systemName: "person")
                Spacer()
            }
            VStack(alignment: .leading, spacing: 10) {
                HStack {
                    Text(imgurComment.author).bold().font(.caption)
                    Spacer()
                    Text("Reply").bold().font(.caption)
                        .padding(.horizontal, 5)
                        .padding(.vertical, 2)
                        .background(Color.gray.opacity(0.3))
                        .cornerRadius(7)
                }
                
                if URL(string: imgurComment.comment) != nil {
                    if image != nil {
                        Image(uiImage: image!)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                        
                    } else {
                        Image(uiImage: UIImage())
                            .onAppear() {
                                let url = URL(string: imgurComment.comment)!
                                cancellable = ImageLoader.shared.loadImage(from: url)
                                    .sink { image = $0 }
                            }
                    }
                }
                Text(imgurComment.comment)
                    .lineLimit(nil)
                    .fixedSize(horizontal: false, vertical: true)
            }
        }
    }
}
