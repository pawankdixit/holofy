//
//  DetailView.swift
//  Holofy
//
//  Created by Pawan Dixit on 1/14/21.
//

import SwiftUI

struct MemeDetailView: View {
    
    let imgurImage: ImgurGalleryItem
    
    @ObservedObject var gallery = ViralGalleryViewModel.shared
    @State private var image: UIImage? = nil
    
    var body: some View {
        VStack(spacing: 0) {
            DetailTopBar(imgurImage: imgurImage)
            memeDetail
            DetailBottomBar(imgurImage: imgurImage)
        }
        .background(Color("Detail"))
        .onAppear() {
            gallery.fetchBestComments(id: imgurImage.id)
        }
        .navigationBarTitle("")
        .navigationBarHidden(true)
        .edgesIgnoringSafeArea(.all)
    }
    
    private var memeDetail: some View {
        ScrollView {
            if imgurImage.is_album ?? false {
                HStack {
                    // Going to display all images in the album
                    ForEach(imgurImage.images!.indices, id: \.self) { index in
                        SpringLoadedImageView(imgurImage: imgurImage.images![index], isVideoEnabled: true, image: $image)
                    }
                }
            } else {
                SpringLoadedImageView(imgurImage: imgurImage, visibleRect: .zero, image: $image)
            }
            
            VStack(alignment: .leading, spacing: 15) {
                HStack {
                    Text("\(imgurImage.views) views").bold()
                    Spacer()
                }.padding()
                
                
                VStack(spacing: 30) {
                    HStack {
                        Text("Best Comments").bold()
                        Spacer()
                        Image(systemName: "plus.bubble")
                        Text("Comment").bold()
                    }
                    
                    if gallery.model.bestComments[imgurImage.id] != nil {
                        ForEach(gallery.model.bestComments[imgurImage.id]!.indices, id:\.self) { index in
                            CommentView(imgurComment: gallery.model.bestComments[imgurImage.id]![index])
                        }
                    }
                }
                .padding()
                .background(Color("Comment"))
            }
            .foregroundColor(.white)
        }
    }
}
