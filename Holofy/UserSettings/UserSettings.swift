//
//  UserSettings.swift
//  Holofy
//
//  Created by Pawan Dixit on 1/15/21.
//

import Foundation

class UserSettings: ObservableObject {
    
    static let shared = UserSettings()
    
    enum HomeLayout: Int {
        case waterfall
        case card
    }
    
    enum HomeContent: Int {
        case viral
        case feed
    }
    
    @Published var homeLayout : HomeLayout = HomeLayout(rawValue: UserDefaults.standard.integer(forKey: "home_layout")) ?? HomeLayout.waterfall {
        didSet {
            UserDefaults.standard.setValue(homeLayout.rawValue, forKey: "home_layout")
        }
    }
    
    @Published var homeContent = HomeContent.viral
}
